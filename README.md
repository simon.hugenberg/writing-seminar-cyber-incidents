# End User Cyber Incidents fsQCA

## Description
This project was made for the Writing Seminar "How to Write an A IS Paper: Reflections, lessons learned, and guidelines". It was used to gather and calibrate the data of the [European Repository of Cyber Incidents](https://eurepoc.eu/) (EuRepoC) which was utilized in the empirical analysis of the WIP paper "The Perfect Storm of Cyber Incidents". Specifically, the paper focused on cyber incidents that targeted end users.

## Installation
To get started, just open the jupyter notebook "consumerDataReader.ipynb" located in this project's main directory in your IDE of choice. It will require an import of the python package pandas.\
Input and Output files are already provided in the given folders, so you may also edit or use those.

## Usage
The input folder contains the data from EuRepoC that has been used for the Seminar. It was downloaded at 17th April 2024.\
You may want to replace the file with a newer version from EuRepoC. In that case make sure to update the jupyter notebook with the new input file name or give the input file the same name as the previous file.

The jupyter notebook "consumerDataReader.ipynb" in the main directory can be used to read the input file and create output files in both csv and excel format.\
The csv files can be used in the [fsQCA software](https://sites.socsci.uci.edu/~cragin/fsQCA/software.shtml) to perform fsQCA.\
The jupyter notebook contains cells to create multiple output files. The one that was used in "The Perfect Storm of Cyber Incidents" is QCAData.csv. The other outputs may be used for additional analysis or simply inspection into the dataset.\
Furthermore, the jupyter notebook provides the opportunity to inspect the data and calculate additional metrics such as the Relevance of Necessity (RoN) which is not provided by the aforementioned fsQCA software.